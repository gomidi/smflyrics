package main

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/gomidi/midi/mid"
	"gitlab.com/metakeule/config"
)

var (
	cfg = config.MustNew(
		"smflyrics",
		"1.5.0",
		"extracts lyrics from a SMF file, tracks are separated by an empty line",
	)

	argFile = cfg.NewString(
		"file",
		"the SMF file that is read in",
		config.Shortflag('f'),
		config.Required,
	)

	argTrack = cfg.NewInt32(
		"track",
		"the track from which the lyrics are taken. -1 means all tracks, 0 is the first, 1 the second etc",
		config.Shortflag('t'),
		config.Default(int32(-1)),
	)

	argIncludeText = cfg.NewBool(
		"text",
		"include free text entries in the SMF file. Text is surrounded by doublequotes",
	)

	argJson = cfg.NewBool(
		"json",
		"output json format",
		config.Shortflag('j'),
	)
)

func main() {
	if err := read(); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())
		os.Exit(1)
	}
}

func read() (err error) {

	var rd = mid.NewReader(mid.NoLogger())

	err = cfg.Run()
	if err != nil {
		fmt.Fprintln(os.Stdout, cfg.Usage())
		return err
	}

	if argJson.Get() {
		j := newJson(rd)
		j.File = argFile.Get()
		j.includeText = argIncludeText.Get()
		j.requestedTrack = int16(argTrack.Get())

		err = rd.ReadSMFFile(argFile.Get())
		if err != nil {
			return err
		}

		bt, err := json.MarshalIndent(j, "", " ")

		if err != nil {
			return err
		}

		fmt.Println(string(bt))
		return nil
	}

	p := newPrinter(rd)
	p.includeText = argIncludeText.Get()
	p.requestedTrack = int16(argTrack.Get())

	return rd.ReadSMFFile(argFile.Get())
}

type printer struct {
	includeText    bool
	requestedTrack int16 // if < 0 : all tracks
}

func newPrinter(rd *mid.Reader) *printer {
	var f printer
	rd.Msg.Meta.Lyric = f.Lyric
	rd.Msg.Meta.Text = f.Text
	rd.Msg.Meta.TrackSequenceName = f.TrackSequenceName
	rd.Msg.Meta.Instrument = f.Instrument
	rd.Msg.Meta.Program = f.Program
	rd.Msg.Meta.EndOfTrack = f.EndOfTrack

	return &f
}

func (f *printer) shouldWrite(track int16) bool {
	return f.requestedTrack < 0 || f.requestedTrack == track
}

func (f *printer) Lyric(p mid.Position, text string) {
	if f.shouldWrite(p.Track) {
		fmt.Print(text + " ")
	}
}

func (f *printer) Text(p mid.Position, text string) {
	if f.shouldWrite(p.Track) && f.includeText {
		fmt.Print("[ " + text + " ] ")
	}
}

func (f *printer) TrackSequenceName(p mid.Position, name string) {
	if f.shouldWrite(p.Track) {
		fmt.Println(fmt.Sprintf("[track: %v]\n", name))
	}
}

func (f *printer) Instrument(p mid.Position, name string) {
	if f.shouldWrite(p.Track) {
		fmt.Println(fmt.Sprintf("[instrument: %v]\n", name))
	}
}

func (f *printer) Program(p mid.Position, name string) {
	if f.shouldWrite(p.Track) {
		fmt.Println(fmt.Sprintf("[program: %v]\n", name))
	}
}

func (f *printer) EndOfTrack(p mid.Position) {
	if f.shouldWrite(p.Track) && p.Track > 0 {
		fmt.Printf("\n\n------------------------\n\n")
	}
}

func newJson(rd *mid.Reader) *Json {
	var t = &Json{}
	t.Tracks = []*Track{&Track{}}
	rd.Msg.Meta.Lyric = t.Lyric
	rd.Msg.Meta.Text = t.Text
	rd.Msg.Meta.TrackSequenceName = t.Track
	rd.Msg.Meta.Instrument = t.Instrument
	rd.Msg.Meta.Program = t.Program
	rd.Msg.Meta.EndOfTrack = t.EndOfTrack

	return t
}

type Track struct {
	Program    string   `json:"program,omitempty"`
	Name       string   `json:"name,omitempty"`
	Instrument string   `json:"instrument,omitempty"`
	Texts      []string `json:"texts,omitempty"`
	Lyrics     []string `json:"lyrics,omitempty"`
	No         int      `json:"no"`
}

func (t *Track) addLyric(l string) {
	t.Lyrics = append(t.Lyrics, l)
}

func (t *Track) addText(tx string) {
	t.Texts = append(t.Texts, tx)
}

type Json struct {
	File           string `json:"file"`
	current        int
	Tracks         []*Track `json:"tracks,omitempty"`
	includeText    bool
	requestedTrack int16 // if < 0 : all tracks
	trackNo        int
}

func (t *Json) shouldWrite(track int16) bool {
	return t.requestedTrack < 0 || t.requestedTrack == track
}

func (t *Json) Lyric(p mid.Position, text string) {
	if t.shouldWrite(p.Track) {
		t.Tracks[t.current].addLyric(text)
	}
}

func (t *Json) Text(p mid.Position, text string) {
	if t.shouldWrite(p.Track) && t.includeText {
		t.Tracks[t.current].addText(text)
	}
}

func (t *Json) Track(p mid.Position, name string) {
	if t.shouldWrite(p.Track) {
		t.Tracks[t.current].Name = name
	}
}

func (t *Json) Instrument(p mid.Position, name string) {
	if t.shouldWrite(p.Track) {
		t.Tracks[t.current].Instrument = name
	}
}

func (t *Json) Program(p mid.Position, name string) {
	if t.shouldWrite(p.Track) {
		t.Tracks[t.current].Program = name
	}
}

func (t *Json) EndOfTrack(p mid.Position) {
	t.trackNo++
	if t.shouldWrite(p.Track) {
		t.Tracks = append(t.Tracks, &Track{No: t.trackNo})
		t.current = len(t.Tracks) - 1
	}
}
