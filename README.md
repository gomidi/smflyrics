# smflyrics
CLI tool to extract lyrics from a Standard MIDI File (SMF)

[![Build Status Travis/Linux](https://travis-ci.org/gomidi/smflyrics.svg?branch=master)](http://travis-ci.org/gomidi/smflyrics)

## Install

    go get -u github.com/gomidi/midi/...
    go get -u github.com/metakeule/config
    go get github.com/metakeule/smflyrics

or download the binary (currently only linux-amd64):
https://github.com/gomidi/smflyrics/releases

## Usage

    smflyrics -f='your-midi-file.mid'

## Documentation

    smflyrics help